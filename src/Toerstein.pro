#-------------------------------------------------
#
# Project created by QtCreator 2015-10-07T19:40:52
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Toerstein
TEMPLATE = app

SOURCES += main.cpp\
        toerstein.cpp \
    codeeditor.cpp \
    toolarea.cpp \
    filesearch.cpp \
    toerstebase.cpp \
    toerstellisense.cpp \
    toerstebaseworker.cpp \
    toerstellisenseworker.cpp \
    tabview.cpp \
    toerstediff.cpp \
    highlighter.cpp

HEADERS += toerstein.h \
    codeeditor.h \
    toolarea.h \
    filesearch.h \
    toerstebase.h \
    toerstellisense.h \
    toerstebaseworker.h \
    toerstellisenseworker.h \
    tabview.h \
    toerstediff.h \
    highlighter.h

mac: {
LIBS += -framework AppKit
OBJECTIVE_SOURCES += misc/macos.mm
HEADERS += misc/macos.h
}
